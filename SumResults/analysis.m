f = fopen('1000.dat', 'r');
A = fscanf(f, '%d');
r = 1000;
area = pi*r^2;

count = zeros(max(A), 1);
for i=1:size(count)
    count(i) = sum(A==i);
end

cumul = cumsum(count)/length(A);

s = @(x) 1 + sum(poisspdf(1:size(cumul), x) .* (cumul-1)');

x = linspace(0, area*2);
y = x;

for i=1:100
    y(i) = s(x(i));
end

clf
hold on
plot(x/area, y, 'b')
plot((1:size(cumul))/area, cumul, 'g')