#ifndef NODE
#define NODE

#include <vector>

class Node {
	public:
		double x, y;
		Node* parent;
		int rank;

		Node();
		Node(double, double);
		void findNeighbours(std::vector<Node*>);
		void connect(Node*);
		Node* findRoot();
};

#endif
