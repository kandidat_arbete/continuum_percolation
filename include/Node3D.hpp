#ifndef NODE3D
#define NODE3D

#include <vector>

class Node3D {
	public:
  double x, y, z;
		Node3D* parent;
		int rank;

		Node3D();
  Node3D(double, double, double);
		void findNeighbours(std::vector<Node3D*>);
		void connect(Node3D*);
		Node3D* findRoot();
};

#endif
