#ifndef ADDITIVE_PERCOLATION
#define ADDITIVE_PERCOLATION

#include <vector>
#include "Node.hpp"

class AdditivePercolation {
  public:
    const double radius;
    const double area;
    const int num_regions;
    std::vector<Node*>* regions;
    Node* const mid;
    Node* const edge;

    AdditivePercolation(double);
    ~AdditivePercolation();
    bool AddBall();
    void output_tikz();
};

#endif
