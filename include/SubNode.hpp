#ifndef SUBNODE
#define SUBNODE

class SubNode {
  public:
    double x, y;
    double lambda;
    double curr_lambda;

    SubNode();
    SubNode(double, double, double);
};

#endif
