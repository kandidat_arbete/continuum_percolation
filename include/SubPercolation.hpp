#ifndef SUBPERCOLATION
#define SUBPERCOLATION

#include <vector>
#include <SubNode.hpp>

class SubPercolation {
  public:
    double radius;
    int num_regions;
    std::vector<SubNode*>* regions;
    double lambdamax;

    SubPercolation(double, double);
    ~SubPercolation();
    double BinarySearch();
    double Dijkstra();
    double DepthFirstSearch(double);
};

#endif
