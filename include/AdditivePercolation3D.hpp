#ifndef ADDITIVE_PERCOLATION3D
#define ADDITIVE_PERCOLATION3D

#include <vector>
#include "Node3D.hpp"

class AdditivePercolation3D {
public:
  const double radius;
  //const double area;
  const int num_regions;
  std::vector<Node3D*>* regions;
  Node3D* const mid;
  Node3D* const edge;
  
  AdditivePercolation3D(double);
  ~AdditivePercolation3D();
  bool AddBall();
  int indexRegion(int, int ,int);
};

#endif
