#include <iostream>
#include <stack>
#include "DFRegion.cpp"

Region** regionlist;

struct Coord {
  int x, y;
};

Region getRegion(int x, int y, int size, double lambda) {
  if (regionlist[size*y + x] == 0) {
    regionlist[size*y + x] = new Region(x, y, lambda);
  }
  return *regionlist[size*y + x];
}


bool depthFirstSearch(int const size, double const lambda) {
  std::stack<Coord> neighbours;
  bool added[size*size];
  for (int i = 0; i < size*size; ++i) {
    added[i] = 0;
  }
  
  Point p = {size/2.0, size/2.0};

  for (int x = -1; x < 2; x++) {
    for (int y = -1; y < 2; y++) {
      Coord c = {size/2 + x, size/2 + y};
      Region t = getRegion(c.x, c.y, size, lambda);
      if (t.overlapsPoint(p)) {
        neighbours.push(c);
        added[size*y + x] = true;
      }
    }
  }

  while (!neighbours.empty()) {
    Coord c = neighbours.top();
    neighbours.pop();

    if ((c.y - size/2.0)*(c.y - size/2.0) + (c.x - size/2.0)*(c.y - size/2.0) >
        0.9*(size/2.0)*(size/2.0)) {
      return true;
    }

    Region t = getRegion(c.x, c.y, size, lambda);

    for (int x = -2; x < 3; x++) {
      for (int y = -2; y < 3; y++) {
        Coord d = {c.x + x, c.y + y};
        if (d.x >= 0 && d.y >= 0 && d.x < size && d.y < size
            && !added[size*d.y + d.x]
            && t.overlaps(getRegion(d.x, d.y, size, lambda))) {
          neighbours.push(d);
          added[size*d.y + d.x] = true;
        }
      }
    }
  }

  return false;
}

  

int main(int argc, char* args[]) {
  if (argc != 3) {
    std::cout << "Needs exactly two arguments!\n";
    return 1;
  }

  int length = atoi(args[1]);
  if (length <= 0) {
    std::cout << "First arg is length: integer and positive.\n";
    return 1;
  }

  double lambda = strtod(args[2], NULL);
  if (lambda <= 0) {
    std::cout << "Second arg is intensity: real and positive.\n";
    return 1;
  }

  regionlist = new Region*[length*length];

  srand(time(NULL));
  std::cout << "Result: " << depthFirstSearch(length, lambda);
}
