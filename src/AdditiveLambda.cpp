#include <iostream>
#include <fstream>
#include <cmath>
#include <ctime>
#include <cstdlib>
#include "AdditivePercolation.hpp"

/**
 * Takes arguments $radius, [$times] and [$datafile].
 *
 * Performs additive percolation with radius $radius, does it $times times and
 * writes the output to $datafile or std::out.
 */
int main(int argc, char* args[]) {
  if (argc < 2) {
    std::cout << "Needs at least one argument!\n";
    return 1;
  }

  double radius = strtod(args[1], nullptr);
  if (radius <= 0) {
    std::cout << "First arg is radius: real and positive.\n";
    return 1;
  }

	int times = 1;
	if (argc >= 3) {
		times = atoi(args[2]);
		if (times < 1) {
			std::cout << "Second arg is repetitions: integer and positive.\n";
			return 1;
		}
	}

	std::ofstream datafile;
	if (argc >= 4) {
		datafile.open(args[3], std::ios::app);
	}

	//Reset the random generator
	srand(time(NULL));

  for(int i = 0; i < times; ++i) {
    double lambda = 0;
    AdditivePercolation p(radius);

    do {
      //How long did it take to add a point?
      lambda += -log(double(rand()) / RAND_MAX);
    } while (!p.AddBall());

    lambda /= p.area;

    if (datafile.is_open()) 
      datafile << lambda << "\n";
    else
      std::cout << lambda << "\n";

    std::cout << double(i*100)/times << "%\n";
  }

	return 0;
}
