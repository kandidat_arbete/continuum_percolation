#include <vector>
#include <random>
#include <cmath>
#include "AreaNode.hpp"

class AreaPercolation {
  public:
    const double length;
    const double area;
    const int num_regions;
    std::vector<AreaNode*>* regions;

    AreaPercolation(double);
    ~AreaPercolation();
    bool AddBall();

  private:
    std::vector<AreaNode*>* getRegion(int x, int y);
};

AreaPercolation::AreaPercolation(int n) :
  length(2*n),
  area(length*length),
  num_regions(n),
  regions(new std::vector<AreaNode*>[num_regions*num_regions])
{}

AreaPercolation::~AreaPercolation() {
	for (int i = 0; i < num_regions*num_regions; ++i) {
		for (AreaNode* n : regions[i]) {
      delete n;
		}
	}
	delete[] regions;
}

bool AreaPercolation::AddBall() {
  //Generate random coordinates
  std::default_random_engine generator;
  std::uniform_real_distribution<double> distribution(0.0, length);

  const double x = distribution(generator);
  const double y = distribution(generator);

  //Get region-coords
  const int i_x = int(x / 2);
  const int i_y = int(y / 2);

  AreaNode* n = new AreaNode(x, y);

  //get neighbours
  std::vector<AreaNode*> neighbours;

  for (int i = -1; i < 2; ++i)
    for (int j = -1; j < 2; ++j)
      for (AreaNode* m : *getRegion(x + i, y + j))
        if ((x - m->x)*(x - m->x) + (y - m->y)*(y - m->y) <= 4)
          neighbours.push_back(m);

  //return true if percolating
  if(n.connect(neighbours))
    return true;

  //If the node added area, remember it.
  if (n.area != 0)
    getRegion(x, y)->push_back(n);

  return false;
}

std::vector<AreaNode*>* getRegion(int x, int y) {
  return &regions[ (y % num_regions) * num_regions + (x % num_regions) ];
}
