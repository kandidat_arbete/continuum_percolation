#include <vector>
#include <cmath>
#include <cstdlib>
#include <cstdio>
#include "Node3D.hpp"
#include "AdditivePercolation3D.hpp"

/*
 * Initializes and allocates.
 */
AdditivePercolation3D::AdditivePercolation3D(double r) :
    radius(r),
    //    area(M_PI * radius * radius),
    num_regions(radius + 2),
    regions(new std::vector<Node3D*>[(int) pow(num_regions,3)]),
    mid(new Node3D),
    edge(new Node3D) {}

/*
 * Frees all memory.
 */
AdditivePercolation3D::~AdditivePercolation3D() {
  for (int i = 0; i<pow(num_regions,3); ++i) {
    for (Node3D* n : regions[i]) {
      delete n;
    }
  }
  delete[] regions;
  delete mid;
  delete edge;
}
/*
 *
 */
int AdditivePercolation3D::indexRegion( int x, int y, int z) {
  return num_regions*num_regions*z+x+y;
}
/*
 * Adds a ball to the percolation. Returns true if the percolation percolates.
 */
bool AdditivePercolation3D::AddBall() {
  //Generate coordinates for the new point
  const double r = pow((double(rand()) / RAND_MAX), 1.0/3.0) * radius;
  const double cos_theta = 2 * (double(rand()) / RAND_MAX) - 1;
  const double omega = 2 * M_PI * (double(rand()) / RAND_MAX);

  const double x = r * sqrt(1-pow(cos_theta,2)) * cos(omega);
  const double y = r * sqrt(1-pow(cos_theta,2)) * sin(omega);
  const double z = r * cos_theta;

  //Get the integer-coords for the region
  const int i_x = floor((num_regions + x)/2);
  const int i_y = floor((num_regions + y)/2);
  const int i_z = floor((num_regions + z)/2);

  //Create the new node
  Node3D* n = new Node3D(x, y, z);

  //Add node to region
  regions[indexRegion(i_x, i_y, i_z)].push_back(n);

  //Connect node to edge/mid
  if (r < 1) 
    n->connect(mid);
  if (r > radius - 1) 
    n->connect(edge);

  //Check for neighbours
  for (int i = -1; i < 2; ++i)
    for (int j = -1; j < 2; ++j)
      for (int k = -1; k < 2; ++k)
        if (i_x + i >= 0 && i_x + i < num_regions &&
            i_y + j >= 0 && i_y + j < num_regions &&
            i_z + k >= 0 && i_z + k < num_regions)
          n->findNeighbours(regions[indexRegion(i_x+i, i_y+j, i_z+k)]);

  return mid->findRoot() == edge->findRoot();
}
