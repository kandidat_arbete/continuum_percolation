#include <limits>
#include <SubNode.hpp>


SubNode::SubNode() :
  x(0),
  y(0),
  lambda(0),
  curr_lambda(0)
{}

SubNode::SubNode(double _x, double _y, double _l) :
  x(_x),
  y(_y),
  lambda(_l),
  curr_lambda(std::numeric_limits<double>::infinity())
{}
