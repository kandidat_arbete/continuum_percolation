#include <cmath>

class AreaNode {
	public:
    static AreaPercolation* percolation = nullptr;

    //Coords
		double x, y;

    //Disjoint forest
		AreaNode* parent;
		int rank;

    //The area of the node
    double area;

    //The area of the tree from the node and down (only correct if root!)
    double tot_area;

    //The times we cross the big edge to reach parent. 
    //EXAMPLE: if parent->x is 0.1 and this->x is L - 0.2, then dis_x is 1,
    //if they are directly connected to each other.
    int displacement_x, displacement_y;

		AreaNode(double, double);
    void setPercolation(AreaPercolation* p);
		bool connect(std::vector<AreaNode*>);
		AreaNode* findRoot();

  private:
    static double calc_area(std::vector<AreaNode*>);
    bool single_connect(AreaNode*);
    void join_to(AreaNode*);
};

AreaNode::AreaNode(double _x, double _y) :
  x(_x),
  y(_y),
  parent(this),
  rank(0),
  area(0),
  tot_area(0),
  displacement_x(0),
  displacement_y(0)
{}

AreaNode::setPercolation(AreaPercolation p) {
  percolation = p;
}

bool AreaNode::connect(std::vector<AreaNode*> nodes) {
  //Calculate the area of these nodes *without* the new node
  double pre = calc_area(nodes);

  //Calculate the area of these nodes *with* the new node
  nodes.push_back(this);
  double post = calc_area(nodes);
  nodes.pop();

  //The difference is the area of this node.
  this.area = post - pre;
  this.tot_area = this.area;

  //If this node is totally contained, nothing happened.
  if (area == 0) {
    return false;
  }

  //Join all the nodes. Return true if percolation happens
  for(AreaNode* n : nodes)
    if (single_connect(n))
      return true;

  //Percolation did not happen
  return false;
}

bool AreaNode::single_connect(AreaNode* n) {
  //This also ensures that the nodes are directly connected to the root.
  //Therefor dis_x and dis_y is the distance to root!
	AreaNode* myRoot = findRoot();
	AreaNode* otherRoot = n->findRoot();


  //The displacement between the two nodes.
  //If we cross the edge, add to displacement!
  double L = percolation->length;
  int local_x = trunc(2*(x - n->x)/L);
  int local_y = trunc(2*(y - n->y)/L);

  //Displacement from myRoot to otherRoot
  int my2o_x = n.displacement_x + local_x - displacement_x;
  int my2o_y = n.displacement_y + local_y - displacement_y;

  //Check for percolation if already connected.
	if (myRoot == otherRoot) {
    return (my2o_x != 0 || my2o_y != 0);
	}

  //Connect the lower rank to the higher
	if (myRoot->rank < otherRoot->rank)
    myRoot->join_to(otherRoot, my2o_x, my2o_y);
	else if (myRoot->rank > otherRoot->rank)
		otherRoot->join_to(myRoot, -my2o_x, -my2o_y);
	else {
    myRoot->join_to(otherRoot, my2o_x, my2o_y);
		otherRoot->rank += 1;
	}

  //They were not connected from the beginning, so surely no percolation.
  return false;
}

void AreaNode::join_to(AreaNode* n, int dis_x, int dis_y) {
  parent = n;
  n->tot_area += tot_area;

  displacement_x = dis_x;
  displacement_y = dis_y;
}

//This function does not conserve tot_area!
AreaNode* AreaNode::findRoot() {
  if (this != parent) {
    AreaNode* old_parent = parent;
    parent = parent->findRoot();
    displacement_x += old_parent->displacement_x;
    displacement_y += old_parent->displacement_y;
  }

  return parent;
}

double AreaNode::calc_area(std::vector<AreaNode*> nodes) {


}
