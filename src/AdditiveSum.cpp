#include <iostream>
#include <fstream>
#include <cmath>
#include <ctime>
#include <cstdlib>
#include <cstdio>
#include "AdditivePercolation.hpp"

/**
 * Takes arguments $radius, [$times] and [$datafile].
 *
 * Performs additive percolation with radius $radius, does it $times times and
 * writes the output to $datafile or std::out.
 */
int main(int argc, char* args[]) {
  if (argc < 2) {
    std::cout << "Needs at least one argument!\n";
    return 1;
  }

  double radius = strtod(args[1], nullptr);
  if (radius <= 0) {
    std::cout << "First arg is radius: real and positive.\n";
    return 1;
  }

	int times = 1;
	if (argc >= 3) {
		times = atoi(args[2]);
		if (times < 1) {
			std::cout << "Second arg is repetitions: integer and positive.\n";
			return 1;
		}
	}

	std::ofstream datafile;
	if (argc >= 4) {
		datafile.open(args[3], std::ios::app);
	}

	//Reset the random generator
	srand(time(NULL));

  for(int i = 0; i < times; ++i) {
    int num_balls = 0;
    AdditivePercolation p(radius);

    do {
      //How many balls do we have to add?
      num_balls += + 1;
    } while (!p.AddBall());

    if (datafile.is_open()) 
      datafile << num_balls << "\n";
    else
      std::cout << num_balls << "\n";

    std::cout << double(i*100)/times << "%\n";
  }

	return 0;
}
