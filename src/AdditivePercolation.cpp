#include <vector>
#include <cmath>
#include <cstdlib>
#include <cstdio>
#include "Node.hpp"
#include "AdditivePercolation.hpp"

/*
 * Initializes and allocates.
 */
AdditivePercolation::AdditivePercolation(double r) :
    radius(r),
    area(M_PI * radius * radius),
    num_regions(radius + 2),
    regions(new std::vector<Node*>[num_regions*num_regions]),
    mid(new Node),
    edge(new Node) {}

/*
 * Frees all memory.
 */
AdditivePercolation::~AdditivePercolation() {
	for (int i = 0; i < num_regions*num_regions; ++i) {
		for (Node* n : regions[i]) {
      delete n;
		}
	}
	delete[] regions;
  delete mid;
  delete edge;
}

/*
 * Adds a ball to the percolation. Returns true if the percolation percolates.
 */
bool AdditivePercolation::AddBall() {
  //Generate coordinates for the new point
  const double r = sqrt(double(rand()) / RAND_MAX) * radius;
  const double theta = 2 * M_PI * (double(rand()) / RAND_MAX);

  const double x = r * cos(theta);
  const double y = r * sin(theta);

  //Get the integer-coords for the region
  const int i_x = floor((num_regions + x)/2);
  const int i_y = floor((num_regions + y)/2);

  //Create the new node
  Node* n = new Node(x, y);

  //Add node to region
  regions[i_y*num_regions + i_x].push_back(n);

  //Connect node to edge/mid
  if (r < 1) 
    n->connect(mid);
  if (r > radius - 1) 
    n->connect(edge);

  //Check for neighbours
  for (int i = -1; i < 2; ++i)
    for (int j = -1; j < 2; ++j)
      if (i_x + i >= 0 && i_x + i < num_regions &&
          i_y + j >= 0 && i_y + j < num_regions)
        n->findNeighbours(regions[(i_y + j)*num_regions + i_x + i]);

  return mid->findRoot() == edge->findRoot();
}

/**
 * Output tikz code that makes an illustration of the percolation.
 */
void AdditivePercolation::output_tikz() {

  printf("\\begin{figure}\n");
  printf("\\begin{center}\n");
	printf("\\begin{tikzpicture}[scale=%.2f]\n", 5.0/radius);

	printf("\\draw[clip] (0,0) circle [radius=%.2f];\n", radius);
	printf("\\filldraw (0,0) circle [radius=0.1];\n");

	for (int i = 0; i < num_regions*num_regions; ++i) {
		for (Node* n : regions[i]) {

			printf("\\draw[%s] (%.2f, %.2f) circle [radius=1];\n",
        n->findRoot() == mid->findRoot() ? "green" : "black",
        n->x, n->y);

		}
	}

	printf("\\end{tikzpicture}\n");
	printf("\\caption{Continuum percolation with radius %.2f", radius);
  printf(" times larger than the radius of the inner circles.}\n");
	printf("\\end{center}\n\\end{figure}\n");
}
