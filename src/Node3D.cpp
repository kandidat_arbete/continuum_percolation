#include "Node3D.hpp"
#include <vector>
#include <cmath>

/**
 * Constructs Node with defualt xyz coords.
 */
Node3D::Node3D() :
  x(0),
  y(0),
  z(0),
  parent(this),
  rank(0)
{}

/**
 * Constructs Node with given coords.
 */
Node3D::Node3D(double _x, double _y, double _z) :
  x(_x),
  y(_y),
  z(_z),
  parent(this),
  rank(0)
{}

/**
 * Iterates over a list and adds any close enough neighbours.
 */
void Node3D::findNeighbours(std::vector<Node3D*> list) {
  for (Node3D* n : list) 
    if (pow((n->x - x),2) + pow((n->y - y),2) + pow((n->z - z),2)  < 4)
			connect(n);
}

void Node3D::connect(Node3D* n) {
	Node3D* myRoot = findRoot();
	Node3D* otherRoot = n->findRoot();

	if (myRoot == otherRoot)
		return;

	if (myRoot->rank < otherRoot->rank)
		myRoot->parent = otherRoot;
	else if (myRoot->rank > otherRoot->rank)
		otherRoot->parent = myRoot;
	else {
		otherRoot->parent = myRoot;
		myRoot->rank += 1;
	}
}

Node3D* Node3D::findRoot() {
	if (this != parent)
		parent = parent->findRoot();

	return parent;
}
