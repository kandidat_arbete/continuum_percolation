#include <iostream>
#include <fstream>
#include <cmath>
#include <ctime>
#include <cstdlib>
//#include <sys/time.h>
#include "SubPercolation.hpp"

/*typedef unsigned long long timestamp_t;

static timestamp_t get_timestamp ()
{
  struct timeval now;
  gettimeofday (&now, NULL);
  return  now.tv_usec + (timestamp_t)now.tv_sec * 1000000;
}
*/
/**
 * Takes arguments $radius, [$times] and [$datafile].
 *
 * Performs additive percolation with radius $radius, does it $times times and
 * writes the output to $datafile or std::out.
 */
int main(int argc, char* args[]) {
  if (argc < 2) {
    std::cout << "Needs at least one argument!\n";
    return 1;
  }
  
  double radius = strtod(args[1], nullptr);
  if (radius <= 0) {
    std::cout << "First arg is radius: real and positive.\n";
    return 1;
  }
  
  int times = 1;
  if (argc >= 3) {
    times = atoi(args[2]);
    if (times < 1) {
      std::cout << "Second arg is repetitions: integer and positive.\n";
      return 1;
    }
  }
  //Reset the random generator
  srand(time(NULL));

  //timestamp_t dij_t = 0;
  //timestamp_t bin_t = 0;

  for(int i = 0; i < times; ++i) {
    SubPercolation p(4,radius);
    //timestamp_t t0 = get_timestamp();
    double lambdaDij = p.Dijkstra();
    //timestamp_t t1 = get_timestamp();
    //double lambdaBin = p.BinarySearch();
    //timestamp_t t2 = get_timestamp();

    //dij_t += t1 - t0;
    //bin_t += t2 - t1;

    
    std::cout << lambdaDij << std::endl;
    std::cout << double(i*100)/times << "%\n";
  }

  //std::cout << "Dijkstra time: " << dij_t << std::endl;
  //std::cout << "Binary time:   " << bin_t << std::endl;

  return 0;
}
