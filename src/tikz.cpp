#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <ctime>

int main(int argc, char* args[]) {
  if (argc != 3) {
    std::cout << "Needs exactly two arguments!\n";
    return 1;
  }

  int length = atoi(args[1]);
  if (length <= 0) {
    std::cout << "First arg is length: integer and positive.\n";
    return 1;
  }

  int num = atoi(args[2]);
  if (num <= 0) {
    std::cout << "Second arg is #circles: integer and positive.\n";
    return 1;
  }

  //Seed generator
  srand(time(NULL));

  //Populate the arrays
  double x[num], y[num];

  for (int i = 0; i < num; i++) {
    x[i] = length * ((float) rand()) / RAND_MAX;
    y[i] = length * ((float) rand()) / RAND_MAX;
  }

  //Output tikz
  std::cout << "\\begin{figure}\n" << "\\begin{center}\n";
  std::cout << "\\begin{tikzpicture}[scale=" << 5.0/length << "]\n";

  printf("\\draw[clip] (%d,%d) rectangle (%d,%d);\n", 0, 0, length, length);

  for (int i = 0; i < num; i++) {
    printf("\\draw (%.2f, %.2f) circle [radius=1];\n", x[i], y[i]);
  }

  std::cout << "\\end{tikzpicture}\n";
  std::cout << "\\caption{Continuum percolation with length " << length << ".}\n";
  std::cout << "\\end{center}\n" << "\\end{figure}\n";

  return 0;
}
