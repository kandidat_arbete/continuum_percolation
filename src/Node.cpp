#include "Node.hpp"
#include <vector>

/**
 * Constructs Node with defualt xy coords.
 */
Node::Node() :
  x(0),
  y(0),
  parent(this),
  rank(0)
{}

/**
 * Constructs Node with given coords.
 */
Node::Node(double _x, double _y) :
  x(_x),
  y(_y),
  parent(this),
  rank(0)
{}

/**
 * Iterates over a list and adds any close enough neighbours.
 */
void Node::findNeighbours(std::vector<Node*> list) {
  for (Node* n : list) 
		if ((n->x - x)*(n->x - x) + (n->y - y)*(n->y - y) < 4)
			connect(n);
}

void Node::connect(Node* n) {
	Node* myRoot = findRoot();
	Node* otherRoot = n->findRoot();

	if (myRoot == otherRoot)
		return;

	if (myRoot->rank < otherRoot->rank)
		myRoot->parent = otherRoot;
	else if (myRoot->rank > otherRoot->rank)
		otherRoot->parent = myRoot;
	else {
		otherRoot->parent = myRoot;
		myRoot->rank += 1;
	}
}

Node* Node::findRoot() {
	if (this != parent)
		parent = parent->findRoot();

	return parent;
}
