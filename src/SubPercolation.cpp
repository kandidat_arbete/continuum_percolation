#include <vector>
#include <random>
#include <algorithm>
#include <unordered_map>
#include <stack>
#include <limits>
#include <SubPercolation.hpp>
#include <SubNode.hpp>
#include <queue>
#include <stack>
#include <iostream>
#include <chrono>

SubPercolation::SubPercolation(double max, double r) :
  radius(r),
  num_regions(radius + 2),
  regions(new std::vector<SubNode*>[num_regions*num_regions]),
  lambdamax(max)
{
  unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
  std::default_random_engine generator(seed);
  std::poisson_distribution<int> poisson(lambdamax*4);
  std::uniform_real_distribution<double> uniform(0.0,1.0);


  for (int i = 0; i < num_regions*num_regions; ++i) {
    int k = poisson(generator);
    for (int j = 0; j < k; ++j) {
      int i_x = i % num_regions;
      int i_y = i / num_regions;
      double x = 2 * (i_x + uniform(generator)) - num_regions;
      double y = 2 * (i_y + uniform(generator)) - num_regions;
      double l = lambdamax * uniform(generator);
      SubNode* n = new SubNode(x, y, l);
      regions[i].push_back(n);
    }
  }
}

SubPercolation::~SubPercolation() {
  for (int i = 0; i < num_regions*num_regions; ++i) {
    for (SubNode* n : regions[i]) {
      delete n;
    }
  }
  delete[] regions;
}

/**
 * Checks if edge is connected to mid by an intensity smaller than lambda,
 * and if so returns the lambda required this time to reach the edge.
 */
double SubPercolation::DepthFirstSearch(double lambda) {
  std::stack<SubNode*> list;
  std::unordered_map<SubNode*, bool> added;

  for (int i = 0; i < 2; ++i) { //The four mid-regions
    for (int j = 0; j < 2; ++j) {
      for (SubNode* n : regions[(num_regions/2-i)*num_regions+num_regions/2-j]){
        if ((n->x)*(n->x) + (n->y)*(n->y) < 1 && n->lambda < lambda) {
          n->curr_lambda = n->lambda;
          list.push(n);
          added[n] = true;
        }
      }
    }
  }

  //Begin DFS
  while (!list.empty()) {
    SubNode* n = list.top();
    list.pop();

    //Stop if we find a way.
    if ((n->x)*(n->x) + (n->y)*(n->y) > radius*radius) {
      return n->curr_lambda;
    }

    //Loop over the 9 closest regions.
    int n_x = floor((num_regions + n->x)/2);
    int n_y = floor((num_regions + n->y)/2);
    for (int dx = -1; dx < 2; ++dx) {
      for (int dy = -1; dy < 2; ++dy) {
        int m_x = n_x + dx;
        int m_y = n_y + dy;

        //But only if the region exists.
        if (m_x >= 0 && m_y >= 0 && m_x < num_regions && m_y < num_regions) {

          //Check each node in the region if it is connected and not added.
          for (SubNode* m : regions[m_y * num_regions + m_x]) {
            if (pow(n->x - m->x, 2) + pow(n->y - m->y, 2) < 4 && !added[m]
                && m->lambda < lambda) {
              //Update curr_lambda
              m->curr_lambda = std::max(n->curr_lambda, m->lambda);

              //Add node to list
              list.push(m);
              added[m] = true;
            }
          }
        }
      }
    }
  }

  //If we don't reach the edge we return inf.
  return std::numeric_limits<double>::infinity();
}


double SubPercolation::BinarySearch() {
  double max = lambdamax;
  double min = 0;
  const double precision = 1e-8;
  double result = 0;

  while (max - min > precision) {
    double mid = (max + min)/2;
    result = DepthFirstSearch(mid);

    if (result <= mid) {
      //DFS found a way. Decrease max.
      max = result;
    } else {
      //DFS did not find a way. Increase min.
      min = mid;
    }
  }

  return (max+min)/2;
}

class SubNodeComp
{
public:
  bool operator() (const SubNode* lhs, const SubNode* rhs) const
  {
    return (lhs->curr_lambda>rhs->curr_lambda);
  }
};
double SubPercolation::Dijkstra() {
  std::priority_queue<SubNode*,std::vector<SubNode*>,SubNodeComp> pq;
  for ( int i =0; i<2; ++i) {
    for ( int j =0; j<2; ++j) {
      for (SubNode* n : regions[(num_regions/2-i)*num_regions+num_regions/2-j]) 
        if ((n->x - 0)*(n->x - 0) + (n->y - 0)*(n->y - 0) < 1) {
          n->curr_lambda = n->lambda;
          pq.push(n);
        }
    }
  }
  while (!pq.empty()){
    SubNode* n = pq.top();
    pq.pop();
    if( pow(n->x,2) + pow(n->y,2) > pow(radius,2)) 
      return n->curr_lambda;
    int i_x = floor((num_regions + n->x)/2);
    int i_y = floor((num_regions + n->y)/2);
    int index = i_y*num_regions+i_x;
    for (int i = -1; i<2; ++i) {
      for (int j = -1; j<2; ++j) {
        for (SubNode* neighbor : regions[index+num_regions*j+i]) {
          if( pow(neighbor->x - n->x,2) + pow(neighbor->y - n->y,2) < 4) {;
            if ( neighbor->curr_lambda > n->curr_lambda)  {
              neighbor->curr_lambda = std::max(neighbor->lambda,n->curr_lambda);
              pq.push(neighbor);
            }
          }
        }
      }
    }
  }
  return std::numeric_limits<double>::infinity();
}
    
