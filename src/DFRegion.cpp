#include <vector>
#include <cmath>
#include <cstdlib>
#include <ctime>
#include <iostream>

struct Point {
  double x, y;
};

int Poisson(double l) {
  int x = 0;
  double p = exp(-l);
  double s = p;

  double u = ((double) rand()) / RAND_MAX;
  while (u > s) {
    x++;
    p = p * l/x;
    s += p;
  }
  return x;
}

class Region {
  int x, y;
  static double const area = 2;
  static double const length = 1.41421356237;

  public:
    std::vector<Point> list;

    Region(int, int, double);
    bool overlaps(Region);
    bool overlapsPoint(Point);
};

Region::Region(int a, int b, double lambda) {
  x = a;
  y = b;

  int k = Poisson(lambda*area);

  std::cout << "k is " << k << "\n";

  for (int i = 0; i < k; ++i) {
    Point p;
    p.x = (x + ((double) rand() / RAND_MAX))*length; 
    p.y = (y + ((double) rand() / RAND_MAX))*length; 
    list.push_back(p);
  }
}

bool Region::overlaps(Region r) {
  std::vector<Point> l = r.list;
  for (std::vector<Point>::iterator u = list.begin(); u != list.end(); ++u) {
    for (std::vector<Point>::iterator v = l.begin(); v != l.end(); ++v) {
      double xd = (*u).x - (*v).x;
      double yd = (*u).y - (*v).y;
      if (xd*xd + yd*yd < 4) {
        return true;
      }
    }
  }
  return false;
}

bool Region::overlapsPoint(Point p) {
  std::cout << "Checking point (" << x << ", " << y << ")\n";
  for (std::vector<Point>::iterator i = list.begin(); i != list.end(); ++i) {
    std::cout << "\tpoint at (" << i->x << ", " << i->y << ")\n";
    double xd = (*i).x - p.x*length;
    double yd = (*i).y - p.y*length;
    if (xd*xd + yd*yd < 1) {
      return true;
    }
  }
  return false;
}


