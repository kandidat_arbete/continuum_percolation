IDIR = include
ODIR = build
EDIR = bin
CFLAGS=-I$(IDIR) -Wall -Wfatal-errors -O3 -std=c++0x -static-libstdc++
CXX=g++

#All C++ files in 'src' that contains a main-function.
EXES = AdditiveLambda AdditiveSum AdditiveSum3D Sub

#All .hpp files in 'include'
_DEPS = AdditivePercolation.hpp Node.hpp AdditivePercolation3D.hpp Node3D.hpp SubNode.hpp SubPercolation.hpp 
DEPS = $(patsubst %,$(IDIR)/%,$(_DEPS))

#All files in 'src' that doesn't contain a main function (i.e. is a class)
_OBJ = Node.o AdditivePercolation.o Node3D.o AdditivePercolation3D.o SubNode.o SubPercolation.o
OBJ = $(patsubst %,$(ODIR)/%,$(_OBJ))

### OBS: NEDANFÖR HÄR BOR DRAKAR, VAR PÅ DIN VAKT!
### (Det vill säga, ändra varsamt.)


#Default target, make all mains.
all: $(EXES)

#Compiles all .o files
$(ODIR)/%.o: src/%.cpp $(DEPS)
	$(CXX) -c -o $@ $< $(CFLAGS)

#Needs all classes and it's own .cpp file
$(EXES): %: $(OBJ) $(ODIR)/%.o
	$(CXX) -o $(EDIR)/$@ $^ $(CFLAGS) $(LIBS)

setup:
	vcs-select g++ _g++_4.9.0
	vcs-select gcc _gcc_4.9.0

#Maked needed directories and cleans 'bin' and 'build'
clean:
	mkdir -p $(ODIR)
	mkdir -p $(EDIR)
	rm -f $(ODIR)/*.o
	rm -f $(INCDIR)/*~
	rm -f $(EDIR)/*
